import { ArrayFormsPage } from './app.po';

describe('array-forms App', () => {
  let page: ArrayFormsPage;

  beforeEach(() => {
    page = new ArrayFormsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
