export interface Equipo {
    nombre: string;
    jugadores: Jugadores[];
};

export interface Jugadores {
    nombre: string;
    dorsal: number;
};
