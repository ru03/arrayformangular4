import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControlName, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  form: FormGroup;
  dorsales = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  constructor(private _fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this._fb.group({
      'equipo': ['', [Validators.required]],
      'jugadores': this._fb.array([
        this.initJugadoresForm()
      ])
    })
  }

  initJugadoresForm() {
    return this._fb.group({
      'nombre': ['', [Validators.required]],
      'dorsal': [1, [Validators.required]]
    })
  }

  addJugadoresForm() {
    let control = <FormArray>this.form.controls['jugadores'];
    // Añadimos un nuevo formulario al array
    control.push(this.initJugadoresForm());
  }

  removeJugador(index: number) {
    let control = <FormArray>this.form.controls['jugadores'];
    // Eliminamos posición del array jugador
    control.removeAt(index);
  }

  guardar() {
    console.log(this.form);
  }
}
